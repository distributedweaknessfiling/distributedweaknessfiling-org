# Distributed Weakness Filing

The Distributed Weakness Filing project is an effort to modernize and improve the security identifier ecopsystem which is currently based on CVE. 

## Getting a CVE security identifier

[https://iwantacve.org/](https://iwantacve.org/)

## Press material

Some press material is available at [https://distributedweaknessfiling.org/press-pack/](https://distributedweaknessfiling.org/press-pack/)

## GitHub content

[https://github.com/distributedweaknessfiling/](https://github.com/distributedweaknessfiling/)
