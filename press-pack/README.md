# Distributed Weakness Filing Project Press Pack

What is the DWF?

## History of the DWF

Previous attempts to work with the legacy MITRE CVE ID system.

## Why the DWF isn't a CVE Numbering Authority (CNA)

## Problems with the Legacy MITRE CVE ID system

### What is the MITRE backend?

Kurt Seifried: I was on the board and I don't know. All I know is MITRE takes CVE data from the cvelist GitHub repo, feeds it into some system on MITREs end and then recommits it into GitHub. Sometimes it removes line feeds, although it's not clear if this is an automated or manual process. If someone from MITRE would like to clarify or explain what the backend was we'd love to update this information. 

### Where's the CVEs?

By YEAR we see the following trend over 10 years:

Year|Number of Legacy CVE ID's assigned|REJECT (assigned and then removed)
--- | --- | ---
2010|5025|148
2011|4591|223
2012|5419|408
2013|6121|510
2014|8289|558
2015|7911|607
2016|9209|1216
2017|14356|2040
2018|15588|961
2019|15304|1093
2020|15925|583

This data was pulled on 2021-04-08 from allitems.csv (https://cve.mitre.org/data/downloads/), the second column is assigned legacy CVE IDs (no REJECT or RESERVED), the third column is all the legacy CVE IDs marked as REJECT'ed.

### Duplicate entries

One concern people have is that people will submit duplicate security identifier requests for existing CVEs. If notified of a duplicate the DWF will check the NVD database to confirm the duplicate and we will then mark our CVE as a duplicate since we can correct this much faster and easier than MITRE can. 

So far as of 2021-04-19 this has not been a significant problem even though there are 3 duplicated entries:

#### Example 1
CVE-2021-1000001 assigned by the DWF on Mar 8 07:12:10 2021

CVE-2021-26806 assigned by MITRE on Apr 14 14:00:43 2021

So MITRE was over a month late and MITRE could have easily found this in the DWF database.

#### Example 2

CVE-2021-1000006 assigned by the DWF on Mar 16 11:39:05 2021 +0000

CVE-2021-28543 assigned by MITRE on Tue Mar 16 15:00:44 2021 +0000

So 3+ hours late and MITRE could have easily found this in the DWF database.

#### Example 3

CVE-2021-1000007 Mar 18 16:23:10 2021

CVE-2021-29932 Thu Apr 1 05:00:39 2021

So MITRE was 2 weeks late and MITRE could have easily found this in the DWF database.

It appears that MITRE is either scraping the DWF database for entries, or taking so long to assign CVEs that people are giving up and coming to the DWF project, or both.
